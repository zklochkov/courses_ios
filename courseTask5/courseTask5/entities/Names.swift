//
//  Names.swift
//  courseTask5
//
//  Created by Zakhar Klochkov on 02.12.2019.
//  Copyright © 2019 Zakhar Klochkov. All rights reserved.
//

import Foundation

enum Names: String, CaseIterable {
    case eddard, robert, jaime, catelyn, cersei, daenerys, jorah, viserys, jon, sansa, arya, robb, theon, bran, joffrey, sandor, tyrion
    
    static func getRandom() -> Names {
        return self.allCases.randomElement()!
    }
}
