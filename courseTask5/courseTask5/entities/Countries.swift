//
//  Countries.swift
//  courseTask5
//
//  Created by Zakhar Klochkov on 02.12.2019.
//  Copyright © 2019 Zakhar Klochkov. All rights reserved.
//

import Foundation

enum Countries: String, CaseIterable {
    case belarus = "Беларусь"
    case russia = "Россия"
    case america = "США"
    case italy = "Италия"
    case india = "Индия"
    case turkey = "Турция"
    
    static func getRandom() -> String {
        guard let randomCountry = self.allCases.randomElement() else { return "БОМЖ"}
        return randomCountry.rawValue
    }
}
