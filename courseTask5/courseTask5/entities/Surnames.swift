//
//  Surnames.swift
//  courseTask5
//
//  Created by Zakhar Klochkov on 02.12.2019.
//  Copyright © 2019 Zakhar Klochkov. All rights reserved.
//

import Foundation

enum Surnames: String, CaseIterable {
    case stark, baratheon, lannister, targaryen, snow, greyjoy, clegane, drogo
    
    static func getRandom() -> Surnames {
        return self.allCases.randomElement()!
    }
}
