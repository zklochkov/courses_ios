//
//  Patronymics.swift
//  courseTask5
//
//  Created by Zakhar Klochkov on 02.12.2019.
//  Copyright © 2019 Zakhar Klochkov. All rights reserved.
//

import Foundation

enum Patronymics: String, CaseIterable {
    case eddardson, robertson, jaimeson, jorahson, viserysson, jonson,  robbson, theonson, branson, joffreson, sandorson, tyrionson
    
    static func getRandom() -> Patronymics {
        return self.allCases.randomElement()!
    }
}
