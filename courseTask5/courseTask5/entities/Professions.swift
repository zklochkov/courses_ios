//
//  Professions.swift
//  courseTask5
//
//  Created by Zakhar Klochkov on 02.12.2019.
//  Copyright © 2019 Zakhar Klochkov. All rights reserved.
//

import Foundation

enum Professions: String, CaseIterable {
    case developer = "Разработчик"
    case designer = "Дизайнер"
    case productManager = "Продукт менеджер"
    case businessAnalyst = "Бизнес-Аналитик"
    
    static func getRandom() -> String {
        return self.allCases.randomElement()!.rawValue
    }
}
