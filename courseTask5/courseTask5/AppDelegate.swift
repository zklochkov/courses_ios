//
//  AppDelegate.swift
//  courseTask5
//
//  Created by Zakhar Klochkov on 01.12.2019.
//  Copyright © 2019 Zakhar Klochkov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

