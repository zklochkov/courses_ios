//
//  ViewController.swift
//  courseTask5
//
//  Created by Zakhar Klochkov on 01.12.2019.
//  Copyright © 2019 Zakhar Klochkov. All rights reserved.
//
import Foundation
import UIKit

class ViewController: UIViewController {
    
    typealias JobSeeker = (surname: String, name: String, patronymic: String, age: Int)
    
    private var jobSeekers: [JobSeeker] = []
    private var countryOfSeeker: String = ""
    
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var ageAndResidency: UILabel!
    @IBOutlet weak var profession: UILabel!
    @IBOutlet weak var experience: UILabel!
    @IBOutlet weak var hourRate: UILabel!
    @IBOutlet weak var weekRate: UILabel!
    @IBOutlet weak var aboutUser: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
    }
    
    @IBAction func payButtonPressed(_ sender: Any) {
        let salary = Int.random(in: 100...500)
        self.weekRate.text = "\(salary)"
        self.hourRate.text = "\(salary / 40)"
    }
    
    @IBAction func changeUserButtonPressed(_ sender: Any) {
        self.fillFormAtRandomSeeker()
        self.fillAbout()
    }
    
    func initialize(){
        while self.jobSeekers.count < 10 {
            self.jobSeekers.append((surname: String(Substring(Surnames.getRandom().rawValue)),
                               name: String(Substring(Names.getRandom().rawValue)),
                               patronymic: String(Substring(Patronymics.getRandom().rawValue)),
                               age: Int.random(in: 18...65)))
        }
    }
    
    func fillFormAtRandomSeeker() {
        guard let jobSeeker = self.jobSeekers.randomElement() else { return }
        
        self.countryOfSeeker = self.countryOfSeeker.isEmpty ? Countries.getRandom() : self.countryOfSeeker 
        self.fullName.text = "\(jobSeeker.name.capitalized) \(jobSeeker.surname.capitalized) \(jobSeeker.patronymic.capitalized)"
        
        self.ageAndResidency.text = "\(jobSeeker.age), \(countryOfSeeker)"
        
        self.profession.text = Professions.getRandom()
        self.fillExperience()
    }
    
    func fillExperience() {
        let experienceValue = Int.random(in: 0...20)
        switch experienceValue {
        case 0:
            self.experience.text = "Без опыта"
        case 1:
            self.experience.text = "Опыт \(experienceValue) год"
        case 2...4:
            self.experience.text = "Опыт \(experienceValue) года"
        default:
            self.experience.text = "Опыт \(experienceValue) лет"
        }
    }
    
    func fillAbout() {
        self.aboutUser.text = ""
        self.aboutUser.insertText("\(self.fullName.text!)\n\(self.ageAndResidency.text!)\n\(self.profession.text!)\n\(self.experience.text!)\nЗарплата:\nВ неделю - \(self.weekRate.text!)\nВ час - \(self.hourRate.text!)")
    }
}
